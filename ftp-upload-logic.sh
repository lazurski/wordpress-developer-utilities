#! /bin/sh
# This is for shitty production servers without git or shell access.
# It uploads all application logic (that is without wp-content/uploads and database of course)
# Adjust below variables to match your configuration.

source ftp.conf

lftp -c "
	set ftp:ssl-allow no;
	set ftp:list-options -a;
	open ftp://$USER@$HOST/;
	cd $RDIR/$SUBDIR;
	lcd $LDIR/$SUBDIR;
	mirror	--reverse \
			--delete \
			--use-cache \
			--verbose \
			--parallel=2 \
			--exclude-glob wp-content/uploads/ \
			--exclude-glob .git/
"