#!/bin/bash

SCRIPT_PATH=$(dirname $(readlink -f $BASH_SOURCE))

# Project configuration
source $SCRIPT_PATH/project.conf

# Read php configuration, namely DB_USER, DB_PASSWORD, DB_NAME and DB_HOST
eval "$(php -q $SCRIPT_PATH/export-wp-config.php)"

if [ -a $1 ]
then
	# Guess remote url
	if grep --silent $URL_PRODUCTION $1
	then
		# "It's a production database."
    awk "{gsub(\"$URL_PRODUCTION\", \"$URL_DEVEL\"); print}" $1 \
    | mysql --user="$DB_USER" --host="$DB_HOST" --password $DB_NAME
	elif grep --silent $URL_STAGE $1
	then
		# "It's a staging database."
    awk "{gsub(\"$URL_STAGE\", \"$URL_DEVEL\"); print}" $1 \
    | mysql --user="$DB_USER" --host="$DB_HOST" --password $DB_NAME
	# else "No guess."
  fi
fi
