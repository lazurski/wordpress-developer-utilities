echo "Export begins" >&2
SCRIPT_PATH=$(dirname $(readlink -f $BASH_SOURCE))
echo "Reading project configuration..." >&2
source $SCRIPT_PATH/project.conf
echo "Reading wordpress configuration..." >&2
# Read php configuration, namely DB_USER, DB_PASSWORD, DB_NAME and DB_HOST
eval "$(php -q $SCRIPT_PATH/export-wp-config.php)"
STAMP=`date +%F-%T`
SQL_DEVEL=$PROJECT_NAME-devel-$STAMP.sql
SQL_STAGE=$PROJECT_NAME-stage-$STAMP.sql
SQL_PRODUCTION=$PROJECT_NAME-production-$STAMP.sql

# Good idea?
# if [ -z "$LZRSKI_ENVIRON" ]
# then LZRSKI_ENVIRON="PRODUCTION"

case "$LZRSKI_ENVIRON" in
	"DEVEL")
		SQL_DUMP=$SQL_DEVEL
		;;
	"STAGE")
		SQL_DUMP=$SQL_STAGE
		;;
	"PRODUCTION")
		SQL_DUMP=$SQL_PRODUCTION
		;;
	*)
		echo "Wrong environment set in wp-config-site.php"
		exit 1
		;;
esac

echo "Dumping database to $SQL_DUMP" >&2
mysqldump \
	--user="$DB_USER" \
	--password="$DB_PASSWORD" \
	--host="$DB_HOST" \
	"$DB_NAME" \
	> $SQL_DUMP

if [ $LZRSKI_ENVIRON != "DEVEL" ]
then
	exit 0
fi

echo "Migration to STAGE ($URL_STAGE)" >&2
awk "{gsub(\"$URL_DEVEL\", \"$URL_STAGE\"); print}" $SQL_DUMP > $SQL_STAGE

if [ $URL_PRODUCTION ]
	echo "Migration to PRODUCTION ($URL_PRODUCTION)" >&2
	then awk "{gsub(\"$URL_DEVEL\", \"$URL_PRODUCTION\"); print}" $SQL_DUMP > $SQL_PRODUCTION
fi
