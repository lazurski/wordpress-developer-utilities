#! /bin/sh

XHPIZE=~/www/xhpize

PROJECT_PATH=~/www/example/
PHP_THEME_DIR='example'
XHP_THEME_DIR='exmaple-xhp'

PHP_THEME_NAME='Example Theme'
XHP_THEME_NAME='XHP Example Theme' #Obsolate?

read -p "About to dry running rsync. Make sure you know what you are doing! (ENTER)"
rsync -rvn --delete --backup --backup-dir $PROJECT_PATH/rsync-backup/ \
	$PROJECT_PATH/application/wp-content/themes/$XHP_THEME_DIR/ \
	$PROJECT_PATH/application/wp-content/themes/$PHP_THEME_DIR/ \
	| less

read -n 1 -p "Proceed? (Y/N): "
if [ $REPLY != Y -a $REPLY != y ]; then
	echo "No? Ok. Bye!"
	exit 1
fi
rsync -r --delete --backup --backup-dir $PROJECT_PATH/xhp-backup/ \
	$PROJECT_PATH/application/wp-content/themes/$XHP_THEME_DIR/ \
	$PROJECT_PATH/application/wp-content/themes/$PHP_THEME_DIR/

read -p "Ok. Now are going to see what xhpize will do to your files. Are you ready? (ENTER)"
find \
	$PROJECT_PATH/application/wp-content/themes/$BASE_THEME_NAME \
	-name '*.php' \
	-type f \
	-print0 \
	| xargs -0 ../xhpize -id \
	| less

read -n 1 -p "Proceed? (Y/N): "
if [ $REPLY != Y -a $REPLY != y ]; then
	echo "No? Ok. Bye!"
	exit 1
fi
find \
	$PROJECT_PATH/application/wp-content/themes/$PHP_THEME_DIR \
	-name '*.php' \
	-type f \
	-print0 \
	| xargs -0 ../xhpize -i

read -p "Hard parts done. Now let's change 'compiled' theme's name. You gonna see how style.css will look like. (ENTER)"
TMP_STYLE=$PROJECT_PATH/xhp-backup/style.tmp
gawk \
	"\
	BEGIN { IGNORECASE=1 } \
	{print gensub(/Theme Name: .*/, \"Theme Name: $VAR\", \"g\")} \
	"
	$PROJECT_PATH/application/wp-content/themes/$PHP_THEME_DIR/style.css \
	> $TMP_STYLE
less $TMP_STYLE

read -n 1 -p "Good? (Y/N): "
if [ $REPLY != Y -a $REPLY != y ]; then
	echo "No? Ok. Bye!"
	exit 1
fi
mv \
	$TMP_STYLE \
	$PROJECT_PATH/application/wp-content/themes/$PHP_THEME_DIR/style.css
