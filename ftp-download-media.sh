#! /bin/sh
# This is for shitty production servers without git or shell access.
# It downloads all application assets (i.e. wp-content/uploads).
# Also you will probabily have to manually migrate database.
# Adjust variables in ftp.conf to match your configuration.

source ftp.conf

lftp -c "
	set ftp:ssl-allow no;
	set ftp:list-options -a;
	open ftp://$USER@$HOST/;
	cd $RDIR/$SUBDIR;
	lcd $LDIR/$SUBDIR;
	mirror	--delete \
		--use-cache \
		--verbose \
		--parallel=4 \
		--exclude-glob .git/
"

echo "Don't forget to migrate database :)"